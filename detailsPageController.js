
function detailsPageController(countryName) {
    
    $.get('https://restcountries.eu/rest/v2/all')
        .then(function (data) {
            allData = data;
            $(function () {
                
                $('#login-container').hide();
                $('#reg-container').hide();
        
                var username = JSON.parse(sessionStorage.getItem('loggedUser'));
        
                var country = allData.find(function(country) {
                    return country.name == countryName;
                });
                var countryCurrency = '';
                country.currencies.map(function(currency) {
                    countryCurrency += currency.name;
                })
                country.currency = countryCurrency;
                console.log(country);
                var source = $('#countryTemplate').html();
                var template = Handlebars.compile(source);
                var html = template({ country: [country] });
                $('main').html(html);
                
            })
        });
        
}

