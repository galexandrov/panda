
function nextPageController() {
    const ENTER_KEYCODE = 13;
    var allData = [];

    function showVisited() {
        var visited = JSON.parse(localStorage.getItem('visited'));
        if (!visited) {
            visited = [];
        }

        var source = $('#visitedTemplate').html();
        var template = Handlebars.compile(source);
        var html = template({ counties: visited });
        $('#visited-countries').html(html);
        $("td.name").on('click', function() {
            location.replace('#'+$(this).parent().attr("id"));
        });
    }
    $.get('https://restcountries.eu/rest/v2/all')
        .then(function (data) {
            allData = data;
            $(function () {
                location.replace("#next");
                $('#login-container').hide();
                $('#reg-container').hide();
        
        
                var country = $('#country').val();
                var username = JSON.parse(sessionStorage.getItem('loggedUser'));
        
                var source = $('#next').html();
                var template = Handlebars.compile(source);
                var html = template({ allData: allData });
                $('#search-countries').html(html);

                $('#country').on('keyup', function(e) {
                    if (e.keyCode == 13) {
                        console.log('enter');
                        var value = $(this).val()
                        var visited = JSON.parse(localStorage.getItem('visited'));
                        if (!visited) {
                            visited = [];
                        }
                        visited.push(allData.find(function(country) {
                            return country.name == value;
                        }))
                        localStorage.setItem('visited', JSON.stringify(visited))
                        showVisited();
                    }
                })
                showVisited();
            })
        });
        
}

