var usersCountry = (function () {

    var nextId = 1;

    function Country(countryName, username) {
        this.id = nextId++;
        this.countryName = countryName;
        this.username = username;
    }

    function AllCounties() {
        if (localStorage.getItem("allCounties") != null) {
            this.counties = JSON.parse(localStorage.getItem("allCounties"));
        } else {
            this.counties = [];
        }
    }

    AllCounties.prototype.addCounties = function(countryName, username){
        var country = new Country (countryName, username);
        this.counties.unshift(country); 
        localStorage.setItem("allCounties", JSON.stringify(this.counties));
    };

    AllCounties.prototype.getCounties= function (username) {
        return this.counties.filter(function (c) {
            return c.username == username;
        })
    };

    
    return new AllCounties()
})()