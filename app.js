
$(function () {
    function router() {
        var page = location.hash.slice(1);

        switch (page) {
            case 'login':
                loginController();
                break;
            case 'register':
                registerController();
                break;
            case 'next':
                nextPageController();
                break;
            default:
                detailsPageController(page);
                break;
        }
    }

    window.addEventListener('hashchange', router);
    router();
})