
var userStorage = (function () {
    function User(fName, sName, username, password, email) {
        this.fName = fName;
        this.sName = sName;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    function UserStorage() {
        if (localStorage.getItem("users") != null) {
            this.users = JSON.parse(localStorage.getItem("users"));
        } else {
            this.users = [];
            // this.users.push(new User("Ivan","Ivanov","vankata94","ivan123","ivan@abv.bg"));
            this.users.push(new User("Ivan", "Ivanov", "vankata21", "ivan123", "ivan3@abv.bg"))
        }
    }

    UserStorage.prototype.registerUser = function (firstName, secondName, username, password, email) {
        if ((typeof firstName !== "string") && (firstName.trim().length <= 0)) {
            alert("Invalid first name. Please enter, again!")
            return;
        }

        if ((typeof secondName !== "string") && (secondName.trim().length <= 0)) {
            alert("Invalid second name. Please enter, again!");
            return;
        }

        var findUser = this.users.find(function (user) {
            return user.username == username
        });
        if (findUser != null) {
            alert("Тhere is such a username. Please enter again!");
            return;
        }
        if (password.trim().length <= 0) {
            alert("Invalid password. Please enter, again!");
            return;
        }

        var newUser = new User(firstName, secondName, username, password, email);
        console.log(newUser)
        this.users.push(newUser);
        localStorage.setItem("users", JSON.stringify(this.users));
        return true;

    }

    UserStorage.prototype.loginUser = function (username, password) {
        var findUser = this.users.findIndex(function (user) {
            return user.username == username && user.password == password;
        });
        // console.log(findUser)
        if (findUser >= 0) {
            localStorage.setItem("users", JSON.stringify(this.users));
            return true;
        } else {
            return false;
        }
    }
    return new UserStorage();
})();

