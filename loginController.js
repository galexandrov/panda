
function loginController() {
    $(function () {
        location.replace("#login");
        $('#reg-container').hide();

        $('#login-btn').on('click', function (event) {
            event.originalEvent.preventDefault()

            var username = $("#lUsername").val();
            var password = $("#lPassword").val();

            if (userStorage.loginUser(username, password)) {
                sessionStorage.setItem('isLogged', true);
                sessionStorage.setItem('loggedUser', JSON.stringify(username));
                
                var source = $('#next').html();

                $('main').html(source);
                $('#login-container').hide();
                $('#reg-container').hide();
                $("#lUsername").val("");
                $("#lPassword").val("");
                location.replace("#next");
            } else {
                alert("Enter again! Wrong user")

                $("#lUsername").val("");
                $("#lPassword").val("");
                location.replace("#login");
                $('#login-container').show();
                $('#reg-container').hide();
            }
        });
        $('#no-acc').on('click', function (event) {
            event.originalEvent.preventDefault();
            location.replace("#register");
            $('#login-container').hide();
            $('#reg-container').show();
        });
    })

}



