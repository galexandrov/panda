function registerController() {
    $(function () {
        $('#reg-btn').on('click', function (event) {
            event.originalEvent.preventDefault();

            var firstName = $("#rFirstName").val();
            var secondName = $("#rSecondName").val();
            var username = $("#Username").val();
            var password = $("#Password").val();
            var emailAddres = $("#Email").val();
            // console.log(firstName, secondName, username, password, emailAddres)

            if (userStorage.registerUser(firstName, secondName, username, password, emailAddres)) {
                $('main').html()
                location.replace("#login");
                $('#login-container').show();
                $("#rFirstName").val("");
                $("#rSecondName").val("");
                $('#rUsername').val("");
                $("#rPassword").val("");
                $("#rEmail").val("");
            } else {
                location.replace("#register");
                $('#login-container').hide();
                $('#reg-container').show();

                $("#rFirstName").val("");
                $("#rSecondName").val("");
                $('#rUsername').val("");
                $("#rPassword").val("");
                $("#rEmail").val("");
            }

        });

    });
};