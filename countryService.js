function load(country) {
    return new Promise(function (resolve, reject) {
        const HTTP_OK = 200;
        var xhr;

        if (XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject();
        }

        xhr.open('GET', 'https://restcountries.eu/rest/v2/all', true);
        xhr.send(null);
        xhr.addEventListener('load', function () {
            if (xhr.status >= HTTP_OK && xhr.status < 300) {      
                var data = JSON.parse(xhr.responseText);
                var countries = [];
                for (i = 0; i < data.list.length; i++) {
                    countries.push({
                        flag: data.list[i].weather[0].main,
                        name: (data.list[i].temp.day - 273).toFixed(1),
                        region: data.list[i].pressure,
                        time: date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear(),
                        capital: data.list[i].weather[0].icon
                    });
                }
                resolve(weather);
                resolve(countries);
            } else {
                reject(xhr.statusText);
            }
        });
    });
}
